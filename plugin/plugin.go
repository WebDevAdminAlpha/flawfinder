package plugin

import (
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks the filename and makes sure it has a c/c++ extension.
func Match(path string, info os.FileInfo) (bool, error) {
	switch strings.TrimPrefix(filepath.Ext(info.Name()), ".") {
	case "c", "cc", "cpp", "c++", "cp", "cxx":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("flawfinder", Match)
}
